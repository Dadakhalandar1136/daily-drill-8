const fs = require('fs');
const path = require('path');

/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.

 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

const data = {
    "employees": [
        {
            "id": 23,
            "name": "Daphny",
            "company": "Scooby Doo"
        },
        {
            "id": 73,
            "name": "Buttercup",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 93,
            "name": "Blossom",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 13,
            "name": "Fred",
            "company": "Scooby Doo"
        },
        {
            "id": 89,
            "name": "Welma",
            "company": "Scooby Doo"
        },
        {
            "id": 92,
            "name": "Charles Xavier",
            "company": "X-Men"
        },
        {
            "id": 94,
            "name": "Bubbles",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 2,
            "name": "Xyclops",
            "company": "X-Men"
        }
    ]
}

function employeesCallback(data) {
    fs.writeFile(path.join(__dirname, './data.json'), JSON.stringify(data), (error) => {
        if (error) {
            console.error(error);
        } else {
            fs.readFile(path.join(__dirname, './data.json'), 'utf-8', (error, data) => {
                if (error) {
                    console.error(error);
                } else {
                    console.log('Data from data.json read successfully');
                    const dataToBeConverted = JSON.parse(data);
                    dataForIds(dataToBeConverted, [2, 13, 23], (error, data) => {
                        if (error) {
                            console.error(error);
                        } else {
                            fs.writeFile(path.join(__dirname, './dataForIds.json'), JSON.stringify(data), (error) => {
                                if (error) {
                                    console.error(error);
                                } else {
                                    console.log("Retrieved data for ids successfull");
                                    groupBaseOnCompanies(dataToBeConverted, (error, data) => {
                                        if (error) {
                                            console.error(error);
                                        } else {
                                            fs.writeFile(path.join(__dirname, './groupBaseOnCompanies.json'), JSON.stringify(data), (error) => {
                                                if (error) {
                                                    console.error(error);
                                                } else {
                                                    console.log("Group based on the companies is successfull");
                                                    companyPowerpuff(dataToBeConverted, (error, data) => {
                                                        if (error) {
                                                            console.error(error);
                                                        } else {
                                                            fs.writeFile(path.join(__dirname, './companyPowerpuff.json'), JSON.stringify(data), (error) => {
                                                                if (error) {
                                                                    console.error(error);
                                                                } else {
                                                                    console.log("Data for Powerpuff Brigade company fetched successful");
                                                                    removeGivenId(dataToBeConverted, (error, data) => {
                                                                        if (error) {
                                                                            console.error(error);
                                                                        } else {
                                                                            fs.writeFile(path.join(__dirname, './removeGivenId.json'), JSON.stringify(data), (error) => {
                                                                                if (error) {
                                                                                    console.error(error);
                                                                                } else {
                                                                                    console.log("Removed the given Id successful");
                                                                                    sortForCompany(dataToBeConverted, (error, data) => {
                                                                                        if (error) {
                                                                                            console.error(error);
                                                                                        } else {
                                                                                            fs.writeFile(path.join(__dirname, './sortForCompany.json'), JSON.stringify(data), (error) => {
                                                                                                if (error) {
                                                                                                    console.error(error);
                                                                                                } else {
                                                                                                    console.log("Sorted as per the companies successful");
                                                                                                    employeeBirthdayAdd(dataToBeConverted, (error, data) => {
                                                                                                        if (error) {
                                                                                                            console.error(error);
                                                                                                        } else {
                                                                                                            fs.writeFile(path.join(__dirname, './employeeBirthdayAdd.json'), JSON.stringify(data), (error) => {
                                                                                                                if (error) {
                                                                                                                    console.error(error);
                                                                                                                } else {
                                                                                                                    console.log("Birthday added successfully");
                                                                                                                    swapCompanies(dataToBeConverted, (error, data) => {
                                                                                                                        if (error) {
                                                                                                                            console.error(error);
                                                                                                                        } else {
                                                                                                                            fs.writeFile(path.join(__dirname, './swapCompanies.json'), JSON.stringify(data), (error) => {
                                                                                                                                if (error) {
                                                                                                                                    console.error(error);
                                                                                                                                } else {
                                                                                                                                    console.log("Swap the give ids successfull");
                                                                                                                                }
                                                                                                                            });
                                                                                                                        }
                                                                                                                    });
                                                                                                                }
                                                                                                            });
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

function dataForIds(employeeData, employeeIds = [2, 13, 23], callback) {
    const idsFound = Object.entries(employeeData)[0][1].filter((element) => {
        return employeeIds.includes(element.id);
    });
    callback(null, idsFound);
}

function groupBaseOnCompanies(employeeData, callback) {
    const groupDetailsFound = Object.entries(employeeData)[0][1].reduce((accumulator, currentValue) => {
        accumulator[currentValue.company].push(currentValue.name);
        return accumulator;
    }, { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": [] });
    callback(null, groupDetailsFound);
}

function companyPowerpuff(employeeData, callback) {
    const companyDetailsFound = Object.entries(employeeData)[0][1].filter((element) => {
        return element.company.includes('Powerpuff Brigade');
    });
    callback(null, companyDetailsFound);
}


function removeGivenId(employeeData, callback) {
    const removedId = Object.entries(employeeData)[0][1].filter((element) => {
        if (element.id === 2) {
            return false;
        } else {
            return true;
        }
    });
    callback(null, removedId);
}

function sortForCompany(employeeData, callback) {
    const sortData = Object.entries(employeeData)[0][1].sort((start, end) => {
        let stratCompany = start.company;
        let endCompany = end.company;
        if (stratCompany > endCompany) {
            return 1;
        } else if (stratCompany < endCompany) {
            return -1;
        } else {
            if (start.id > end.id) {
                return 1;
            } else {
                return -1;
            }
        }
    });
    callback(null, sortData);
}


function swapCompanies(employeeData, callback) {
    const swapData = Object.entries(employeeData)[0][1].filter((element) => {
        return (element.id === 93 || element.id === 92);
    });
    const swapData93 = { ...swapData[0] };
    const swapData92 = { ...swapData[1] };

    const storeDataSwapped = Object.entries(employeeData)[0][1].map((item) => {
        if (item.id === 92) {
            item.company = swapData93.company;
        }
        if (item.id === 93) {
            item.company = swapData92.company;
        }
        return item;
    })
    callback(null, storeDataSwapped);
}

function employeeBirthdayAdd(employeeData, callback) {
    const getBirthday = Object.entries(employeeData)[0][1].map((element) => {
        if (element.id % 2 === 0) {
            element.birthday = new Date();
        } else {
            element.birthday = "";
        }
        return element;
    })
    callback(null, getBirthday);
}


employeesCallback(data);